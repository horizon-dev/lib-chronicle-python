from cryptography.hazmat.backends import openssl
from cryptography.hazmat.primitives.asymmetric import rsa

from . import __TEXT_ENCODING

from .ioutil import (
    __INT_SIZE,
    __INT2_SIZE,
    __BYTE_ORDER,
    read_bytes,
    int_to_bytes,
    size_to_bytes
)


__KEY_ENCODING_RSACRT_PRIVATE = \
        "RSA_PRIVCRT_KEY".encode(encoding=__TEXT_ENCODING)
__KEY_ENCODING_RSA_PUBLIC = "RSA_PUB_KEY".encode(encoding=__TEXT_ENCODING)


class UnsupportedKeyTypeError(Exception):
    pass


def private_key_from_bytes(buffer):
    offset = [__INT_SIZE]
    key_type = read_bytes(buffer, offset=offset[0])
    offset[0] = offset[0] + __INT_SIZE + len(key_type)
    if key_type != __KEY_ENCODING_RSACRT_PRIVATE:
        raise UnsupportedKeyTypeError()
    else:
        def read_int():
            v = read_bytes(buffer, offset=offset[0])
            offset[0] = offset[0] + __INT_SIZE + len(v)
            return int.from_bytes(v, byteorder=__BYTE_ORDER, signed=True)

        n = read_int() # Modulus
        e = read_int() # Public exponent
        d = read_int() # Private exponent
        p = read_int() # Prime P
        q = read_int() # Prime Q
        dmp1 = read_int() # Prime exponent P
        dmq1 = read_int() # Prime exponent Q
        iqmp = read_int() # Coefficient

        public_nums = rsa.RSAPublicNumbers(e, n)
        private_nums = rsa.RSAPrivateNumbers(
                p, q, d, dmp1, dmq1, iqmp, public_nums)
        return private_nums.private_key(backend=openssl.backend)

def bytes_from_private_key(key):
    buffer = bytearray(size_to_bytes(1))

    def extend(v):
        x = int_to_bytes(v)
        buffer.extend(size_to_bytes(len(x)))
        buffer.extend(x)

    buffer.extend(size_to_bytes(len(__KEY_ENCODING_RSACRT_PRIVATE)))
    buffer.extend(__KEY_ENCODING_RSACRT_PRIVATE)

    privnums = key.private_numbers()

    extend(privnums.public_numbers.n) # Modulus
    extend(privnums.public_numbers.e) # Public exponent
    extend(privnums.d) # Private exponent
    extend(privnums.p) # Prime P
    extend(privnums.q) # Prime Q
    extend(privnums.dmp1) # Prime exponent P
    extend(privnums.dmq1) # Prime exponent Q
    extend(privnums.iqmp) # Coefficient

    return bytes(buffer)

def public_key_from_bytes(buffer):
    offset = [0]
    key_type = read_bytes(buffer, offset=offset[0])
    offset[0] = offset[0] + __INT_SIZE + len(key_type) + __INT2_SIZE
    if key_type != __KEY_ENCODING_RSA_PUBLIC:
        raise UnsupportedKeyTypeError()
    else:
        def read_int():
            v = read_bytes(buffer, offset=offset[0])
            offset[0] = offset[0] + __INT_SIZE + len(v)
            return int.from_bytes(v, byteorder=__BYTE_ORDER, signed=True)

        e = read_int() # Public exponent
        n = read_int() # Modulus
        public_nums = rsa.RSAPublicNumbers(e, n)
        return public_nums.public_key(backend=openssl.backend)

def bytes_from_public_key(key):
    buffer = bytearray()

    def extend(v):
        x = int_to_bytes(v)
        buffer.extend(size_to_bytes(len(x)))
        buffer.extend(x)

    buffer.extend(size_to_bytes(len(__KEY_ENCODING_RSA_PUBLIC)))
    buffer.extend(__KEY_ENCODING_RSA_PUBLIC)
    buffer.extend(b'\x00\x00')

    pubnums = key.public_numbers()
    extend(pubnums.e) # Public exponent
    extend(pubnums.n) # Modulus

    return bytes(buffer)
