"""
Handle system types based on https://www.ietf.org/rfc/rfc3651.txt
"""
import inspect
import sys
from abc import (ABC, abstractmethod)

from . import __TEXT_ENCODING as TEXT_ENCODING
from . import keyutil
from .ioutil import (
    __BYTE_ORDER as BYTE_ORDER,
    __INT_SIZE as INT_SIZE,
    __INT2_SIZE as INT2_SIZE,
    read_bytes,
    size_to_bytes
)


class HandleValueType(ABC):

    def __init__(self, val):
        self.val = val

    @classmethod
    def type_name(cls):
        return cls.__name__.upper()

    @classmethod
    @abstractmethod
    def preferred_index(cls):
        pass

    @classmethod
    @abstractmethod
    def decode(cls, buf):
        pass

    @abstractmethod
    def encode(self):
        pass


class StringValueType(HandleValueType):

    @classmethod
    def decode(cls, buf):
        return cls(bytes(buf).decode(encoding=TEXT_ENCODING))

    def encode(self):
        return self.val.encode(encoding=TEXT_ENCODING)


class Desc(StringValueType):

    @classmethod
    def preferred_index(cls):
        return 1


class Email(StringValueType):

    @classmethod
    def preferred_index(cls):
        return 3


class URL(StringValueType):

    @classmethod
    def preferred_index(cls):
        return 2


class Alias(StringValueType):

    @classmethod
    def type_name(cls):
        return 'HS_ALIAS'

    @classmethod
    def preferred_index(cls):
        return 4


class SecKey(StringValueType):

    @classmethod
    def type_name(cls):
        return 'HS_SECKEY'

    @classmethod
    def preferred_index(cls):
        return 301


class PubKey(HandleValueType):

    @classmethod
    def type_name(cls):
        return 'HS_PUBKEY'

    @classmethod
    def preferred_index(cls):
        return 300

    @classmethod
    def decode(cls, buf):
        return cls(keyutil.public_key_from_bytes(buf))

    def encode(self):
        return keyutil.bytes_from_public_key(self.val)


class Admin(HandleValueType):

    class AdminValue():

        def __init__(self, **kwargs):
            self.handle_value = kwargs.get('handle_value', '')
            self.handle_index = kwargs.get('handle_index', '')
            self.add_handle = kwargs.get('add_handle', False) #(0x0001)
            self.delete_handle = kwargs.get('delete_handle', False) #(0x0002)
            self.add_na = kwargs.get('add_na', False) #(0x0004)
            self.delete_na = kwargs.get('delete_na', False) #(0x0008)
            self.modify_value = kwargs.get('modify_value', False) #(0x0010)
            self.delete_value = kwargs.get('delete_value', False) #(0x0020)
            self.add_value = kwargs.get('add_value', False) #(0x0040)
            self.modify_admin = kwargs.get('modify_admin', False) #(0x0080)
            self.remove_admin = kwargs.get('remove_admin', False) #(0x0100)
            self.add_admin = kwargs.get('add_admin', False) #(0x0200)
            self.authorized_read = kwargs.get('authorized_read', False) #(0x0400)
            self.list_handle = kwargs.get('list_handle', False) #(0x0800)
            self.list_na = kwargs.get('list_na', False) #(0x1000)

    @classmethod
    def type_name(cls):
        return 'HS_ADMIN'

    @classmethod
    def preferred_index(cls):
        return 100

    @classmethod
    def decode(cls, buf):
        hvb = read_bytes(buf, offset=INT2_SIZE)
        hv = hvb.decode(TEXT_ENCODING)
        offset = len(hvb) + INT_SIZE + INT2_SIZE
        hi = int.from_bytes(buf[offset:offset + INT_SIZE], BYTE_ORDER)
        offset += INT_SIZE
        mask = int.from_bytes(buf[0:INT2_SIZE], BYTE_ORDER)
        def is_set(pos):
            return bool(mask & (1 << pos))
        return cls(cls.AdminValue(
            handle_value=hv,
            handle_index=hi,
            add_handle=is_set(0),
            delete_handle=is_set(1),
            add_na=is_set(2),
            delete_na=is_set(3),
            modify_value=is_set(4),
            delete_value=is_set(5),
            add_value=is_set(6),
            modify_admin=is_set(7),
            remove_admin=is_set(8),
            add_admin=is_set(9),
            authorized_read=is_set(10),
            list_handle=is_set(11),
            list_na=is_set(12)))

    def encode(self):
        buf = bytearray()
        def set_bit(pos):
            mask = 1 << pos
            set_bit.perms = set_bit.perms | mask
        set_bit.perms = 0
        if self.val.add_handle:
            set_bit(0)
        if self.val.delete_handle:
            set_bit(1)
        if self.val.add_na:
            set_bit(2)
        if self.val.delete_na:
            set_bit(3)
        if self.val.modify_value:
            set_bit(4)
        if self.val.delete_value:
            set_bit(5)
        if self.val.add_value:
            set_bit(6)
        if self.val.modify_admin:
            set_bit(7)
        if self.val.remove_admin:
            set_bit(8)
        if self.val.add_admin:
            set_bit(9)
        if self.val.authorized_read:
            set_bit(10)
        if self.val.list_handle:
            set_bit(11)
        if self.val.list_na:
            set_bit(12)
        buf.extend(set_bit.perms.to_bytes(INT2_SIZE, BYTE_ORDER))
        hval = self.val.handle_value.encode(encoding=TEXT_ENCODING)
        buf.extend(size_to_bytes(len(hval)))
        buf.extend(hval)
        buf.extend(size_to_bytes(self.val.handle_index))
        return bytes(buf)


class Unknown(HandleValueType):

    def __init__(self, val):
        self.val = val

    @classmethod
    def preferred_index(cls):
        return 999

    @classmethod
    def decode(cls, buf):
        return cls(buf)

    def encode(self):
        return self.val


def lookup_class_for_handle_value_type(name):
    classes = inspect.getmembers(sys.modules[__name__], lambda o: \
        inspect.isclass(o) \
        and not inspect.isabstract(o) \
        and hasattr(o, 'type_name'))
    for cls in classes:
        if name == cls[1].type_name():
            return cls[1]
    return Unknown
