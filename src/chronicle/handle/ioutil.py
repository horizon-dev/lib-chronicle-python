import math


__INT_SIZE = 4
__INT2_SIZE = 2
__BYTE_ORDER = 'big'


def read_bytes(buffer, offset=0):
    end = offset + __INT_SIZE
    length = int.from_bytes(
        buffer[offset : end],
        byteorder=__BYTE_ORDER, signed=False)
    return bytes(buffer[end : end + length])

def int_to_bytes(i):
    buffer = bytearray()
    if i.bit_length() % 8 == 0:
        buffer.extend(b'\x00')
    size = math.ceil(i.bit_length() / 8)
    buffer.extend(i.to_bytes(size, __BYTE_ORDER))
    return bytes(buffer)

def size_to_bytes(x):
    return x.to_bytes(__INT_SIZE, __BYTE_ORDER)
