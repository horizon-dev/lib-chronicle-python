import copy
import base64
import io
import os
import time
from urllib import parse

from cryptography.hazmat.backends import openssl
from cryptography.hazmat.primitives import (hashes, hmac, serialization)
from cryptography.hazmat.primitives.asymmetric import (padding, rsa)

__encoding = 'UTF-8'


def __quote(x):
    if type(x) == bytes:
        return parse.quote(x)
    else:
        return parse.quote(x, encoding=__encoding)

def __build_string(
        method,
        url,
        query,
        body,
        nonce,
        timestamp,
        username=False,
        token_id=False):
    with io.StringIO() as buffer:
        buffer.write(method.upper())
        buffer.write('&')
        buffer.write(__quote(url))
        buffer.write('&')

        query = copy.copy(query)

        query.append(('auth_signature_method', 'RSA-SHA512',))
        query.append(('auth_timestamp', str(timestamp),))
        if username:
            query.append(('auth_username', username,))
        elif token_id:
            query.append(('auth_token_id', token_id,))
        query.append(('auth_version', '1.0',))
        query.append(('auth_nonce', nonce,))

        query = [(__quote(k), __quote(v)) for (k, v) in query]
        query.sort(key=(lambda arg: arg[1]))
        query.sort(key=(lambda arg: arg[0]))

        with io.StringIO() as querybuf:
            querybuf.write(query[0][0])
            querybuf.write('=')
            querybuf.write(query[0][1])
            for (k, v,) in query[1:]:
                querybuf.write('&')
                querybuf.write(k)
                querybuf.write('=')
                querybuf.write(v)
            buffer.write(__quote(querybuf.getvalue()))

        buffer.write('&')
        buffer.write(__quote(body))

        return __quote(buffer.getvalue())

def generate_nonce():
    return base64.b64encode(os.urandom(32)).decode(__encoding)

def create_hmac_sha512_signature(
        key,
        method,
        url,
        query,
        token_id,
        body,
        nonce=generate_nonce(),
        timestamp=int(time.time())):
    base = __build_string(
        method,
        url,
        query,
        body,
        nonce,
        timestamp,
        token_id=str(token_id)
    )
    ekey = key.encode(__encoding)
    h = hmac.HMAC(ekey, hashes.SHA512(), backend=openssl.backend)
    h.update(base.encode(__encoding))
    return base64.b64encode(h.finalize()).decode(__encoding)

def verify_hmac_sha512_signature(
        key,
        signature,
        method,
        url,
        query,
        token_id,
        body,
        nonce,
        timestamp):
    base = __build_string(
        method,
        url,
        query,
        body,
        nonce,
        timestamp,
        token_id=str(token_id)
    )
    ekey = key.encode(__encoding)
    h = hmac.HMAC(ekey, hashes.SHA512(), backend=openssl.backend)
    h.update(base.encode(__encoding))
    h.verify(base64.b64decode(signature))

def create_rsa_sha512_signature(
        key,
        method,
        url,
        query,
        username,
        body,
        nonce=generate_nonce(),
        timestamp=int(time.time())):
    base = __build_string(
        method,
        url,
        query,
        body,
        nonce,
        timestamp,
        username=username
    )
    return base64.b64encode(key.sign(
        base.encode(__encoding),
        padding.PSS(
            mgf=padding.MGF1(hashes.SHA512()),
            salt_length=padding.PSS.MAX_LENGTH
        ),
        hashes.SHA512())).decode(__encoding)

def verify_rsa_sha512_signature(
        key,
        signature,
        method,
        url,
        query,
        username,
        body,
        nonce,
        timestamp):
    base = __build_string(
            method,
            url,
            query,
            body,
            nonce,
            timestamp,
            username=username
        )
    key.verify(
        base64.b64decode(signature),
        base.encode(__encoding),
        padding.PSS(
            mgf=padding.MGF1(hashes.SHA512()),
            salt_length=padding.PSS.MAX_LENGTH
        ),
        hashes.SHA512())
