from setuptools import setup, find_packages
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='chronicle',
    version='0.0.2',
    description='Common Python code for the Chronicle platform',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://bitbucket.org/horizon-dev/lib-chronicle-python',
    author='Horizon Digital Economy Research',
    package_dir={'': 'src'},
    packages=find_packages('src'),
    install_requires=['pyopenssl>=17'],
)
